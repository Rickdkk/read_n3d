"""
-Read N3D-
Description: Can be used to read Optotrak binary NDF files to Python
Author:     Rick de Klerk
Contact:    r.de.klerk@umcg.nl
Company:    University Medical Center Groningen
License:    GNU GPLv3.0
Date:       13/08/2018
"""

import numpy as np
from struct import unpack


def merge_chars(chars):
    """Merges list of binary characters to single string"""
    return ''.join([x.decode("utf-8") for x in chars])


def load_n3d(filename, verbose=True):
    """Reads NDI-optotrak data files

    Input
        filename: *.NDF datafile in path or full path
        verbose: default is True, disable to disable printouts
    Output
        data: numpy array with markers with 3D arrays of measurement data in milimeters
        sfrq: sample frequency in Hz"""

    with open(filename, "rb") as f:
        content = f.read()

    filetype = unpack('c', content[0:1])[0]                         # char
    items = unpack('h', content[1:3])[0]                            # int16, number of markers
    subitems = unpack('h', content[3:5])[0]                         # int16, number of dimensions (usually 3)
    numframes = unpack('i', content[5:9])[0]                        # int32, number of frames
    sfrq = unpack('f', content[9:13])[0]                            # float32, sample frequency
    usercomment = merge_chars(unpack('c' * 60, content[13:73]))
    sys_comment = merge_chars(unpack('c' * 60, content[73:133]))
    descrp_file = merge_chars(unpack('c' * 30, content[133:163]))
    cuttoff_frq = unpack('h', content[163:165])
    coll_time = merge_chars(unpack('c' * 10, content[165:175]))
    coll_date = merge_chars(unpack('c' * 10, content[175:185]))
    rest = merge_chars(unpack('c' * 71, content[185:256]))          # padding

    if verbose:
        print("-" * 50)
        print(f'Reading data from {filename}, recorded on {coll_date} at {coll_time} with {sfrq} Hz.')
        print("-" * 50)

    num_total = items * subitems * numframes  # total number of 'samples'
    optodata = np.array(unpack('f' * num_total, content[256:]))

    optodata[optodata <= -10e20] = np.nan  # replace NDF nan with numpy nan
    optodata = np.reshape(optodata, (numframes, items, subitems)).T  # [0] = xyz, [1] = marker, [2] = samples
    return optodata, sfrq


if __name__ == "__main__":  # test function
    print(load_n3d("example.n3d"))
