## Read n3d

Simple script to read binary n3d file for the NDI Optotrak 3020. Written for our (ancient) Optotrak system, probably does not work for newer systems. Could not find any implementation for Python so I made a script to read the binary file and convert the data to a multidimensional numpy array. 

### Prerequisites

Only relies on the built-in module struct and external module numpy in combinatin with Python 3.6x

### Authors

* Rick de Klerk - *Initial work* - [gitlab](https://gitlab.com/rickdkk) - [UMCG](https://www.rug.nl/staff/r.de.klerk/)

### License

This project is licensed under the GNU GPLv3 - see the [LICENSE](LICENSE) file for details
